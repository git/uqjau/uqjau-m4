m4_divert(-1)
# Copyright (C) 1999-2012 Free Software Foundation, Inc.

# This file is part of Autoconf.  This program is free
# software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# Under Section 7 of GPL version 3, you are granted additional
# permissions described in the Autoconf Configure Script Exception,
# version 3.0, as published by the Free Software Foundation.
#
# You should have received a copy of the GNU General Public License
# and a copy of the Autoconf Configure Script Exception along with
# this program; see the files COPYINGv3 and COPYING.EXCEPTION
# respectively.  If not, see <http://www.gnu.org/licenses/>.

# Written by Akim Demaille.

# ----------------------------------------------------------------------
# -- From /usr/share/autoconf/m4sugar/m4sugar.m4 --
# Macro snips from 'm4sugar.m4', with no interesting changes, just adjusted
# for a change in m4 quotes.
# ----------------------------------------------------------------------

# m4_chomp(STRING)
# m4_chomp_all(STRING)
# --------------------
# Return STRING quoted, but without a trailing newline.  m4_chomp
# removes at most one newline, while m4_chomp_all removes all
# consecutive trailing newlines.  Embedded newlines are not touched,
# and a trailing backslash-newline leaves just a trailing backslash.
#
# m4_bregexp is slower than m4_index, and we don't always want to
# remove all newlines; hence the two variants.  We massage characters
# to give a nicer pattern to match, particularly since m4_bregexp is
# line-oriented.  Both versions must guarantee a match, to avoid bugs
# with precision -1 in m4_format in older m4.
m4_define([<{m4_chomp}>],
[<{m4_format([<{[<{%.*s}>]}>], m4_index(m4_translit([<{[<{$1}>]}>], [<{
/.}>], [<{/  }>])[<{./.}>], [<{/.}>]), [<{$1}>])}>])


# TODO/FIXME -- notice m4_bregexp swapped with m4_regexp
m4_define([<{m4_chomp_all}>],
[<{m4_format([<{[<{%.*s}>]}>], m4_regexp(m4_translit([<{[<{$1}>]}>], [<{
/}>], [<{/ }>]), [<{/*$}>]), [<{$1}>])}>])

# m4_esyscmd_s(COMMAND)
# ---------------------
# Like m4_esyscmd, except strip any trailing newlines, thus behaving
# more like shell command substitution.
m4_define([<{m4_esyscmd_s}>],
[<{m4_chomp_all(m4_esyscmd([<{$1}>]))}>])



m4_divert(0)m4_dnl
