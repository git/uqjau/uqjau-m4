m4_divert(-1)

# ====================================================================
# Copyright (c) 2017, 2018 _Tom_Rodman_email@sed
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

# SYNOPSIS
# 
# DESCRIPTION
# 
#     These macros may depend on autoconf's:
#     /usr/share/autoconf/m4sugar/m4sugar.m4.  This lib is used by
#     script '_m4s'.
# 
# NOTES
# 
#     Uses # as m4 comment as GNU m4sugar libraries do.  
# 
#     All macros here should have prefix _M4SUG to indicate dependency
#     on GNU m4sugar libraries.
# 
# 
# FILES
#     Some macros here depend on GNU M4sugar libraries.
#     script: _m4s

# Start of a post:
# Subject: help w/m4_pushdef() and m4_popdef(); review/fix up this macro pls
#
# I have under used the debug and (trace?) options for m4; I'll
# plan to do this.  The macro below was hacked together by trial
# and error. I would like help fixing it up, but want it to use
# m4_pushdef() and m4_popdef(), so I learn their intended usage.
#
# Consider the macro def below, and pls show how I should be doing the
# m4_popdef for _frog_eb438a779e85ca.  I added a suffix to '_frog'
# since I could not undefine it w/m4_popdef() and still get macro to
# work.  I want to get an understanding of how to use m4_pushdef() and
# m4_popdef() in order to chain together several changes to $1.

# _M4SU_NL_TO_GS 
# Give single arg $1, trim leading and trailing newlines, swap all newlines (\n) with ascii GS (\035) char.
# The macro seems to work, but needs review.
# Bug - could not effectively m4_popdef on _frog_eb438a779e85ca.

m4_define(_M4SUG_NL_TO_GS_last_stable,
    [m4_pushdef([_frog_eb438a779e85ca], m4_echo(
    [m4_pushdef([_bar], [m4_chomp_all([m4_expand([$1])])m4_popdef([_bar])])m4_bpatsubst(_bar, [
], [])]))m4_bpatsubst(_frog_eb438a779e85ca, [^[]*], [])])

m4_define(_fum,
    [m4_pushdef([_frog_eb438a779e85ca], m4_echo(
    [m4_pushdef([_bar], [m4_chomp_all([m4_bpatsubst([m4_expand([$1])], [ ]*$, [])])m4_popdef([_bar])])m4_bpatsubst(_bar, [
], [])]))m4_bpatsubst(_frog_eb438a779e85ca, [^[]*], [])])

m4_define(_zot,
    [m4_pushdef([_frog_eb438a779e85ca], m4_echo(
    [m4_pushdef([_bar], [[m4_expand([$1])]m4_popdef([_bar])])m4_bpatsubst(_bar, [
], [])]))m4_bpatsubst(_frog_eb438a779e85ca, [^[]*], [])])

m4_define(_bork0,
    [m4_pushdef([_thud_c4199ee869e0ee], m4_echo(
    [m4_pushdef([_frog_eb438a779e85ca], [[m4_pushdef([_bar], [[m4_expand([$1])]m4_popdef([_bar])])m4_bpatsubsts(_bar, [
], [])]])m4_bpatsubst(_frog_eb438a779e85ca, [^[]*], [])]))m4_bpatsubst(_thud_c4199ee869e0ee, [*$], [])])

m4_define(_bork1,
    [m4_pushdef([_thud_c4199ee869e0ee], m4_echo(
    [m4_pushdef([_frog_eb438a779e85ca], [[m4_pushdef([_bar], [[m4_expand([$1])]m4_popdef([_bar])])m4_bpatsubsts(_bar, [ ]*$, [], [
], [], [^*], [])]])m4_bpatsubst(_frog_eb438a779e85ca, [^[]*], [])]))m4_bpatsubst(_thud_c4199ee869e0ee, [*$], [])])

m4_define(_bork1-branch,
    [m4_pushdef([_eggs_5c9e34b958a61a], m4_echo(
    [m4_pushdef([_thud_c4199ee869e0ee], [[m4_pushdef([_frog_eb438a779e85ca], [[m4_pushdef([_bar], [[m4_expand([$1])]m4_popdef([_bar])])m4_bpatsubsts(_bar, [ ]*$, [], [
], [], [^*], [])]])m4_bpatsubst(_frog_eb438a779e85ca, [^[]*], [])]])m4_bpatsubst(_thud_c4199ee869e0ee, [*$], [])]))m4_bpatsubsts([ZAAA]_eggs_5c9e34b958a61a, ZAAA[*], [])])

m4_define(_bork2,
    [m4_pushdef([_thud_c4199ee869e0ee], m4_echo(
    [m4_pushdef([_frog_eb438a779e85ca], [[m4_pushdef([_bar], [[m4_expand([$1])]m4_popdef([_bar])])m4_bpatsubsts(_bar, [ ]*$, [], [
], [], [^*], [])]])m4_bpatsubst(_frog_eb438a779e85ca, [^[]*], [])]))m4_bpatsubsts([b1754b194835b15a2bae]_thud_c4199ee869e0ee[f19da07337e87b9faaacfe], [*f19da07337e87b9faaacfe], [],[b1754b194835b15a2bae][]*, [])])


# _M4SUG_NL_TO_GS is set to _bork2 and m4_bpatsubsts(_bar, [ ]*$, [], ... fails , so closing brackets must be in column 1
m4_define(_M4SUG_NL_TO_GS,
    [m4_pushdef([_thud_c4199ee869e0ee], m4_echo(
    [m4_pushdef([_frog_eb438a779e85ca], [[m4_pushdef([_bar], [[m4_expand([$1])]m4_popdef([_bar])])m4_bpatsubsts(_bar, [ ]*$, [], [
], [], [^*], [])]])m4_bpatsubst(_frog_eb438a779e85ca, [^[]*], [])]))m4_bpatsubsts([b1754b194835b15a2bae]_thud_c4199ee869e0ee[f19da07337e87b9faaacfe], [*f19da07337e87b9faaacfe], [],[b1754b194835b15a2bae][]*, [])])

m4_define(_qux, [m4_bpatsubst([m4_expand([$1])], [ ]*$, [])])

m4_define(_sob, [m4_bpatsubsts([$1], [ ]*$, [])])

# [m4_bpatsubst([$1], [ ]*$, [])]
m4_define([_M4SUG_bc_1expr], [m4_chomp(m4_esyscmd([ OUT_SCALE=$2 _bc_1expr $1 ]))])
